//Dylan Staniszewski
//September 6, 2018, CSE 002
//This program takes as input time elapsed in seconds and the number of wheel rotations, and gives as output total minutes, total rotations, distance per trip, and total distance.This
public class Cyclometer{
  //main method required for every Java program
  public static void main (String []args){
    
    //input data
    int secTrip1 = 480; //seconds in trip 1
    int secTrip2 = 3220; //seconds in trip 2
    int countTrip1 = 1561; //rotations in trip 1
      int countTrip2 = 9037; //rotations in trip 2
    
    //constants
    double diameter = 27.0, //wheel diameter
    PI = 3.14159, //value of pi
    feetPerMile = 5280, //feet per mile
    inchesPerFoot = 12, //inches per foot
    secondsPerMinute = 60; //seconds in a minute
    double dTrip1, dTrip2, dTotal; //total distances per trip and the grand total.

    System.out.println("Trip 1 took " + secTrip1/secondsPerMinute + " minutes and had " + countTrip1 + " counts.");
    System.out.println("Trip 2 took " + secTrip2/secondsPerMinute + " minutes and had " + countTrip2 + " counts.");
    
    //run calculations, store values.
    dTrip1 = (countTrip1 * diameter * PI)/(inchesPerFoot * feetPerMile);
    dTrip2 = (countTrip2 * diameter * PI)/(inchesPerFoot * feetPerMile);
    dTotal = dTrip1 + dTrip2;
    
    //print output
    System.out.println("Trip 1 was " + dTrip1 + " miles");
	  System.out.println("Trip 2 was " + dTrip2 + " miles");
	  System.out.println("The total distance was "+ dTotal +" miles");

    
  } //end of main method
} //end of class
