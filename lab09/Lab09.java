public class Lab09{
    public static int[] copy(int[] a){ //makes a copy of the input array
        int length = a.length;
        int[] newArray = new int[length];
        for (int i=0; i<length; i++){
            newArray[i] = a[i];
        }
        return newArray;
    }
    
    public static void inverter(int[] a){ //directly inverts the input array
        int length = a.length;
        int temp;
        
        for(int i=0; i <(length/2); i++){
            temp = a[i];
            a[i] = a[(length-1) - i];
            a[(length-1) - i] = temp;
        }
    }
    
    public static int[] inverter2(int[] a){ //makes a copy of the input array and inverts the copy
        int length = a.length;
        int[] newArray = new int[length];
        newArray = copy(a);
        inverter(newArray);
        return newArray;
    }
    
    public static void print(int[] a){ //prints out the input array
        for(int i=0; i<a.length; i++){
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }
    
    public static void main (String []args){ //main method
        int[] array0 = new int[10], array1 = new int[10], array2 = new int[10];
        int[] array3;
        for(int i=0; i<array0.length; i++){
            array0[i] = i;
        }
        array1 = copy(array0);
        array2 = copy(array0);
        inverter(array0);
        System.out.print("Array0: ");
        print(array0);
        System.out.print("Array1: ");
        print(array1);
        System.out.print("Inverted copy of Array1: ");
        print(inverter2(array1));
        System.out.print("Array3: ");
        array3 = inverter2(array2);
        print(array3);
    } 
}