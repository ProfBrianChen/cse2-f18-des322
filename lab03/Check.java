//Dylan Staniszewski
//September 13, 2018
//This program takes as input the total check, the tip percentage, and the number of people and outputs how much each person needs to spend to split the check evenly
import java.util.Scanner;
public class Check {
    //main method required for every java program
    public static void main (String []args){
        double check; //total cost of check
        double tip; //tip percentage
        double divide; //number of ways check is being divided
        double total; //how much each person must pay for the check
        
        //variables for splitting the check
        int dollars; //total dollars
        int dimes; //total dimes (10s of cents)
        int pennies; //total pennies (1s of cents)
        
        Scanner reader = new Scanner(System.in);
        
        System.out.print("How much is the check? Enter as xx.xx: ");
        check = reader.nextDouble(); //input total check
        System.out.print("Please enter the tip percentage as a whole number (Ex: 15% = 15): ");
        tip = reader.nextDouble(); //input tip
        tip = tip/100.0; //convert tip to decimal
        System.out.print("How many ways is the check being divided? ");
        divide = reader.nextDouble(); //input number of ways
        
        check = check + (check * tip); //calculate check + tip
        total = check / divide; //calculate how much each person must spend
        
        dollars = (int)(total); //calculate total dollars
        dimes = (int)(total * 10) % 10; //calculate 10s of cents
        pennies = (int)(total * 100) % 10; //calculate 1s of cents
        
        
        System.out.println("Each person must pay $" + dollars + "." + dimes + pennies + " to evenly split the check.");
    } //end of main method
} //end fo class
