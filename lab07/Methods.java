import java.util.Scanner;
import java.util.Random;
public class Methods{
    public static String noun(){ //generate nouns
        Random rand = new Random();
        int num = rand.nextInt(10);
        String noun = " ";
        switch(num){
            case 0: 
            noun = "dog";
            break;
            case 1:
            noun = "fox";
            break;
            case 2:
            noun = "bear";
            break;
            case 3:
            noun = "chimera";
            break;
            case 4:
            noun = "goat";
            break;
            case 5:
            noun = "zombie";
            break;
            case 6:
            noun = "cyclops";
            break;
            case 7:
            noun = "dragon";
            break;
            case 8:
            noun = "sloth";
            break;
            case 9:
            noun = "tiger";
            break;
            default:
            noun = "yeet";
            break;
        }
        return noun;
    }

    public static String verb(){ //generate verbs
        Random rand = new Random();
        int num = rand.nextInt(10);
        String verb = " ";
        switch(num){
            case 0: 
            verb = "thrust";
            break;
            case 1:
            verb = "drove";
            break;
            case 2:
            verb = "colluded";
            break;
            case 3:
            verb = "beheld";
            break;
            case 4:
            verb = "fled";
            break;
            case 5:
            verb = "overpowered";
            break;
            case 6:
            verb = "wounded";
            break;
            case 7:
            verb = "shredded";
            break;
            case 8:
            verb = "punctured";
            break;
            case 9:
            verb = "declared war against";
            break;
            default:
            verb = "fought";
            break;
        }
        return verb;
    }

    public static String adjective(){ //generate adjectives
        Random rand = new Random();
        int num = rand.nextInt(10);
        String adj = " ";
        switch(num){
            case 0: 
            adj = "drunken";
            break;
            case 1:
            adj = "lazy";
            break;
            case 2:
            adj = "energetic";
            break;
            case 3:
            adj = "sleepy";
            break;
            case 4:
            adj = "disgusted";
            break;
            case 5:
            adj = "apologetic";
            break;
            case 6:
            adj = "disgraceful";
            break;
            case 7:
            adj = "traitorous";
            break;
            case 8:
            adj = "motivated";
            break;
            case 9:
            adj = "narcissistic";
            break;
            default:
            adj = "yeet";
            break;
        }
        return adj;
    }

    public static String object(){ //generate object
        Random rand = new Random();
        int num = rand.nextInt(10);
        String noun = " ";
        switch(num){
            case 0: 
            noun = "potato";
            break;
            case 1:
            noun = "singular electron";
            break;
            case 2:
            noun = "bear";
            break;
            case 3:
            noun = "juice";
            break;
            case 4:
            noun = "scepter";
            break;
            case 5:
            noun = "carrot";
            break;
            case 6:
            noun = "table";
            break;
            case 7:
            noun = "carpet";
            break;
            case 8:
            noun = "Henry VIII's crown";
            break;
            case 9:
            noun = "water bottle";
            break;
            default:
            noun = "the ";
            break;
        }
        return noun;
    }

    public static String adverb(){ //generate adverbs
        Random rand = new Random();
        int num = rand.nextInt(10);
        String noun = " ";
        switch(num){
            case 0: 
            noun = "very";
            break;
            case 1:
            noun = "quite";
            break;
            case 2:
            noun = "awfully";
            break;
            case 3:
            noun = "particularly";
            break;
            case 4:
            noun = "more";
            break;
            case 5:
            noun = "less";
            break;
            case 6:
            noun = "extremely";
            break;
            case 7:
            noun = "hardly";
            break;
            case 8:
            noun = "pretty";
            break;
            case 9:
            noun = "never";
            break;
            default:
            noun = "always";
            break;
        }
        return noun;
    }

    public static void thesis(String subject){ //generate a topic sentence
        System.out.println("The " + adjective() + " " + adjective() + " " + subject + " " + verb() +
            " the " + adjective() + " " + object() + ".");
    }

    public static void helpingSentence(String subject){ //generate the body of the paragraph
        int useIt = 0;
        int numSentences, counter = 1, num;
        Random rand = new Random();
        numSentences = rand.nextInt(4)+1;
        while(counter<numSentences){
            if(counter != 1) useIt = rand.nextInt(2);
            if(useIt==0){
                System.out.println("This " + subject + " was " + adverb() + " " + adjective() + " to " + adjective()
                    + " " + object() + "s.");
            }
            else{
                System.out.println("It was " + adverb() + " " + adjective() + " to " + adjective()
                    + " " + object() + "s.");
            }
            counter++;
        }
    }

    public static void conclusion(String subject){ //generate a concluding sentence
        System.out.println("That " + subject + " " + verb() + " its " + object() + "!");
    }
    
    public static void main (String []args){ //main method
        String answer, subject;
        Scanner reader = new Scanner(System.in);
        subject = noun();
        
        System.out.println("--PHASE ONE--");
        do{

            System.out.println("The " + adjective() + " " + adjective() + " " + noun() + " " + verb() +
                " the " + adjective() + " " + object() + ".");
            System.out.print("Would you like to generate a new sentence? Enter 'y' for yes: ");
            answer=reader.nextLine();
        }while(answer.equalsIgnoreCase("y"));
        System.out.println("--PHASE TWO--");
        //assemble sentence
        thesis(subject);
        helpingSentence(subject);
        conclusion(subject);
    }//end of main method
}