import java.util.Random; //allows random number gen
public class CardGenerator{
    //main method for every java program
    public static void main (String []args) {
        Random rand = new Random(); //random number 
        String suit; //defines suit
        String card; //defines card (Ace-King)               
        int cardNumber = rand.nextInt(52) + 1; //generates random card
        
        if(cardNumber <= 13) {
            suit = "Diamonds";
        } //sets suit to diamonds if between 1-13
        else if(cardNumber <= 26) {
            suit = "Clubs";
            cardNumber = cardNumber - 13;
        } //sets suit to clubs if between 14-26 AND brings number between 1-13 for 'card' definition
        else if(cardNumber <= 39) {
            suit = "Hearts";
            cardNumber = cardNumber - 26;
        } //sets suit to hearts if between 27-39 AND brings number between 1-13 for 'card' definition
        else {
            suit = "Spades";
            cardNumber = cardNumber - 39;
        } //sets suit to spades if between 40-52 AND brings number between 1-13 for 'card' definition
       
        switch (cardNumber) { //giant switch statement that sets 1-13 as Ace-King
            case 1: card = "Ace";
            break;
            case 2: card = "Two";
            break;
            case 3: card = "Three";
            break;
            case 4: card = "Four";            
            break;
            case 5: card = "Five";            
            break;
            case 6: card = "Six";            
            break;
            case 7: card = "Seven";             
            break;
            case 8: card = "Eight";           
            break;
            case 9: card = "Nine";            
            break;
            case 10: card = "Ten";            
            break;
            case 11: card = "Jack";            
            break;
            case 12: card = "Queen";
            break;
            case 13: card = "King";
            break;
            default: card = "Invalid";
            break;
        }
        System.out.println("You have picked the " + card + " of " + suit + "."); //display results
    } //end of method
} //end of class