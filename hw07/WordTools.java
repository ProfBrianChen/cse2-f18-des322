import java.util.Scanner;
public class WordTools{
    public static String enterText(){ //prompt user to enter text
        String text;
        Scanner reader = new Scanner(System.in);
        System.out.print("Enter a sample text: ");
        text=reader.nextLine();
        return text;
    } //end

    public static String printMenu(){  //print menu of options and input choice
        String answer;
        Scanner reader = new Scanner(System.in);

        System.out.println("MENU");
        System.out.println("c - Number of non-whitespace characters");
        System.out.println("w - Number of words");
        System.out.println("f - Find text");
        System.out.println("r - replace all !'s");
        System.out.println("s - Shorten spaces");
        System.out.println("q - Quit");
        System.out.println();

        while(true){
            System.out.print("Choose an option: ");
            answer=reader.nextLine();
            if(answer.equals("c")) break;
            if(answer.equals("w")) break;
            if(answer.equals("f")) break;
            if(answer.equals("r")) break;
            if(answer.equals("s")) break;
            if(answer.equals("q")) break;
            else System.out.println("Error: Invalid entry.");
        }
        return answer;
    } //end

    public static void getNumOfNonWSCharacters(String text){ //determine number of nonwhite characters in the text
        int length = text.length(), nonWSCounter = 0;

        for(int i = 0; i<length; i++){
            if(text.charAt(i) != (' ')){
                nonWSCounter++;
            }
        }
        System.out.println("Number of non whitespace characters: " + nonWSCounter);
    } //end

    public static void getNumOfWords(String text){ //determine total number of words in the text
        int length = text.length(), wordCount = 1;

        for(int i = 0; i<length; i++){
            if(text.charAt(i) == ' '){
                wordCount++;
            }
        }
        System.out.println("Number of words: " + wordCount);
    } //end

    public static void findText(String text, String word){ //finds how many instances of a specified word are in the text
        int length = text.length(), wordCount = 0;
        int wordLength = word.length();
        String temp = "";

        for(int i = 0; i<length; i++){
            if(text.charAt(i) != (' ')){
                temp += text.charAt(i);
                if(temp.equals(word)){
                    wordCount++;
                }
            }
            else{
                temp = "";
            }
        }
        System.out.println("Your word was found " + wordCount + " times.");
    } //end

    public static String replaceExclamation(String text){ //replaces all "!" with "."
        int length = text.length();
        String fullText = "";

        for(int i=0; i<length; i++){
            if(text.charAt(i) == ('!')){
                fullText += ".";
            }
            else{
                fullText += text.charAt(i);
            }
        }
        return fullText;
    } //end
    
    public static String shortenSpace(String text){ //shortens all spaces of 2 or more to 1
        int length = text.length(), whitespaceCounter = 0;
        String fullText = "";
        
        for(int i=0; i<length; i++){
            if(text.charAt(i) == (' ')){
                whitespaceCounter++;
                if(whitespaceCounter>1){
                    
                }
                else{
                    fullText += text.charAt(i);
                }
            }
            else{
                fullText += text.charAt(i);
                whitespaceCounter = 0;
            }
        }
        return fullText;
    } //end
    
    public static void main (String []args){ //main method
        Scanner reader = new Scanner(System.in);
        String word;

        String text = enterText();
        String choice = printMenu();
        if(choice.equals("c")) getNumOfNonWSCharacters(text);
        if(choice.equals("w")) getNumOfWords(text);
        if(choice.equals("f")){
            System.out.print("Enter a word to search for: ");
            word=reader.nextLine();

            findText(text, word);
        }
        if(choice.equals("r")) System.out.println(replaceExclamation(text));
        if(choice.equals("s")) System.out.println(shortenSpace(text));
        if(choice.equals("q")) System.out.println("You chose quit. Have a nice day.");
    } //end of main method
}