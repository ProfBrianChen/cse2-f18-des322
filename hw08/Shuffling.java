import java.util.*;
public class Shuffling{
    public static void printArray(String[] c){ //prints whatever array is input, either the "deck" or the "hand"
        for(int i = 0; i<c.length; i++){ //output value at position i
            System.out.print(c[i] + " ");
        }
        System.out.println();
    } //end

    public static void shuffle(String[] c){ //continuously swaps the position of a random "card" with the value at the 0 position
        Random rand = new Random();
        String temp = " ";
        for(int i = 0; i< 150; i++){ 
            temp = " ";
            int num = rand.nextInt(51) + 1;
            temp = c[num];
            c[num] = c[0];
            c[0] = temp;
        }
    } //end

    public static String[] getHand(String[] c, int index, int numCards){ //generates a "hand" of 5 cards based off of the top 5 cards of the "deck"
        String[] hand = new String[5];
        for(int i = 0; i <5; i++){
            hand[i] = c[index - i];
        }
        return hand;
    } //end

    public static void main(String[] args) { //main method
        Scanner scan = new Scanner(System.in);
        //suits club, heart, spade or diamond 
        String[] suitNames={"C","H","S","D"};    
        String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
        String[] cards = new String[52]; 
        String[] hand = new String[5]; 
        int numCards = 5; 
        int again = 1; 
        int index = 51;
        for (int i=0; i<52; i++){ 
            cards[i]=rankNames[i%13]+suitNames[i/13]; 
            System.out.print(cards[i]+" "); 
        } 
        System.out.println();
        printArray(cards); 
        shuffle(cards); 
        printArray(cards); 
        while(again == 1){
            hand = getHand(cards, index,numCards); 
            printArray(hand);
            index = index - numCards;
            System.out.print("Enter a 1 if you want another hand drawn: "); 
            again = scan.nextInt(); 
        }  
    } //end main
}