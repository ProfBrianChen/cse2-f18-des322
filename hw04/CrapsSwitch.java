import java.util.Scanner;
import java.util.Random;
public class CrapsSwitch{
    //main method for every java program
    public static void main (String []args){
        int dice1 = 0, dice2 = 0, diceTotal = 0; //dice variables
        Random rand = new Random(); //random
        Scanner reader = new Scanner(System.in);
        int answer;
        String slang = " ";

        System.out.println("Would you like to manually input or automatically generate dice?"); 
        System.out.print("Enter '1' for manual and '2' for automatic: ");
        answer=reader.nextInt(); //enter input for dice gen

        switch(answer){
            case 1:
            do{
                System.out.print("Enter a value for dice 1: "); //manually input dice1
                dice1 = reader.nextInt();
            }while(dice1<0 || dice1 > 6);
            do{
                System.out.print("Enter a value for dice 2: "); //manually input dice2
                dice2=reader.nextInt();
            }while(dice2<0 || dice2>6);
            diceTotal = dice1 + dice2;
            break;
            case 2: //auto gen dice
            dice1 = rand.nextInt(6) + 1;
            dice2 = rand.nextInt(6) + 1;
            diceTotal = dice1 + dice2;
            break;
            default: System.out.println("Invalid entry.");
            break;
        }

        switch(diceTotal){ //determine slang term based on doubles/not doubles and total
            case 2: slang = "Snake Eyes";
            break;
            case 3: slang = "Ace Deuce";
            case 4:
            switch(dice1){
                case 1: slang = "Easy Four";
                break;
                case 2: slang = "Hard Four";
                break;
                case 3: slang = "Easy Four";
                break;
            }
            break;
            case 5: slang = "Fever Five";
            break;
            case 6:
            switch(dice1){
                case 1:
                case 2: slang = "Easy Six";
                break;
                case 3: slang = "Hard Six";
                break;
                case 4:
                case 5: slang = "Easy Six";
                break;
            }
            break;
            case 7: slang = "Seven Out";
            break;
            case 8:
            switch(dice1){
                case 2:
                case 3: slang = "Easy Eight";
                break;
                case 4: slang = "Hard Eight";
                break;
                case 5:
                case 6: slang = "Easy Eight";
                break;
            }
            break;
            case 9: slang = "Nine";
            case 10:
            switch(dice1){
                case 4: slang = "Easy Ten";
                break;
                case 5: slang = "Hard Ten";
                break;
                case 6: slang = "Easy Ten";
                break;
            }
            break;
            case 11: slang = "Yo-leven";
            break;
            case 12: slang = "Boxcars";
            break;
        }
        System.out.println("You have rolled " + slang + "."); //output
    }
}