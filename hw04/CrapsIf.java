import java.util.Scanner;
import java.util.Random;
public class CrapsIf{
    public static void main (String []args){
        int dice1 = 0, dice2 = 0, diceTotal = 0;
        Random rand = new Random();
        Scanner reader = new Scanner(System.in);
        Scanner stringReader = new Scanner(System.in);
        String answer;
        String slang = " ";

        System.out.println("Would you like to manually input or automatically generate dice?"); 
        System.out.print("Enter 'm' for manual and 'a' for automatic: ");
        answer=stringReader.nextLine();
        if(answer.equalsIgnoreCase("m")){
            while(true){
                System.out.print("Enter a value for the first dice: ");
                dice1 = reader.nextInt();
                if(dice1>6){
                    System.out.println("Invalid entry, try again.");
                }
                else if(dice1<1){
                    System.out.println("Invalid entry, try again.");
                }
                else break;
            }
            while(true){
                System.out.print("Enter a value for the second dice: ");
                dice2 = reader.nextInt();
                if(dice2>6){
                    System.out.println("Invalid entry, try again.");
                }
                else if(dice2<1){
                    System.out.println("Invalid entry, try again.");
                }
                else break;
            }
            diceTotal = dice1 + dice2;
            
        }
        else if (answer.equalsIgnoreCase("a")){
            dice1 = rand.nextInt(6) + 1;
            dice2 = rand.nextInt(6) + 1;
            System.out.println("Dice 1: " + dice1);
            System.out.println("Dice 2: " + dice2);
            diceTotal = dice1 + dice2;
        }
        else{
            System.out.println("You entered neither. Have a nice day.");
        }
        if(diceTotal == 2){ //if total is 2, this is the slang
                slang = "Snake Eyes";
            }
            else if(diceTotal == 3){ //if total is 3, this is the slang
                slang = "Ace Deuce";
            }
            else if(diceTotal == 4){ //if the result is 4, this is the slang
                if(dice1 != 2){
                    slang = "Easy Four"; //not doubles
                }
                else{
                    slang = "Hard Four"; //doubles
                }
            }
            else if(diceTotal == 5){ //if total is 5, this is the slang
                slang = "Fever Five";
            }
            else if(diceTotal == 6) { //if total is 6, this is the slang
                if(dice1 != 3){   
                    slang = "Easy Six"; //not doubles
                }
                else {
                    slang = "Hard Six"; //doubles
                }
            }
            else if(diceTotal == 7){ //if total is 7, this is the slang
                slang = "Seven Out";
            }
            else if(diceTotal == 8){ //if total is 8, this is the slang
                if(dice1 != 4){
                    slang = "Easy Eight"; //not doubles
                }
                else {
                    slang = "Hard Eight"; //doubles
                }
            }
            else if(diceTotal == 9){ //if total is 9, this is the slang
                slang = "Nine"; 
            }
            else if(diceTotal == 10){ //if total is 10, this is the slang
                if(dice1 != 5){
                    slang = "Easy Ten"; //not doubles
                }
                else{
                    slang = "Hard Ten"; //doubles
                }
            }
            else if(diceTotal == 11){ //if total is 11, this is the slang
                slang = "Yo-leven";
            }
            else if(diceTotal == 12){ //if total is 12, this is the slang
                slang = "Boxcars";
            }
            else{
                
            }
        System.out.println("You have rolled " + slang + ".");
    }
}