import java.util.Scanner;
public class UserInputs{
  //main method
    public static void main (String []args){
        String courseNumber, timesPerWeek, numStudents; //declare variables
        String department, time, amOrPm, instructorName; //declare
        Scanner reader = new Scanner(System.in);
        Scanner stringReader = new Scanner(System.in);
        
        while(true){ //course number input & check
            System.out.print("Input a course number: ");
            courseNumber = reader.nextLine();
            Scanner courseNum = new Scanner(courseNumber);
            if(courseNum.hasNextInt()){
                break;
            }
            else{
                System.out.println("Error: please enter an integer.");
            }
        }
        
        
        while(true){ //department input and check
            System.out.print("Input name of the department: ");
            department = reader.nextLine();
            Scanner depName = new Scanner(department);
            if(depName.hasNextInt()){
                System.out.println("Error: please enter letters.");
            }
            else if(depName.hasNextDouble()){
                System.out.println("Error: please enter letters.");
            }
            else{
                break;
            }
        }
        
        while(true){ //times per week input and check
            System.out.print("Input number of times per week as a whole number: ");
            timesPerWeek = reader.nextLine();
            Scanner timesWeek = new Scanner(timesPerWeek);
            if(timesWeek.hasNextInt()){
                break;
            }
            else{
                System.out.println("Error: please enter an integer.");
            }
        }
        
        while(true){ //time input and check
            System.out.print("Input what time of day the course begins (12 hour system): ");
            time = reader.nextLine();
            Scanner timeStart = new Scanner(time);
            if(timeStart.hasNextInt()){
                break;
            }
            else{
                System.out.println("Error: please enter an integer in the form XX:XX.");
            }
        }
        
        while(true){ //am/pm input and check
            System.out.print("AM or PM? ");
            amOrPm = reader.nextLine();
            Scanner amPm = new Scanner(amOrPm);
            if(amPm.hasNextInt()){
                System.out.println("Error: please enter letters.");
            }
            else if(amPm.hasNextDouble()){
                System.out.println("Error: please enter letters.");
            }
            else{
                break;
            }
        }
        
        while(true){ //instructor name input and check
            System.out.print("Input the instructor's name: ");
            instructorName = reader.nextLine();
            Scanner instName = new Scanner(instructorName);
            if(instName.hasNextInt()){
                System.out.println("Error: please enter letters.");
            }
            else if(instName.hasNextDouble()){
                System.out.println("Error: please enter letters.");
            }
            else{
                break;
            }
        }
        
        while(true){ //number of students input and check
            System.out.print("Input the number of students in the class: ");
            numStudents = reader.nextLine();
            Scanner numberStudents = new Scanner(numStudents);
            if(numberStudents.hasNextInt()){
                break;
            }
            else{
                System.out.println("Error: please enter an integer.");
            }
        }
        
        System.out.println("<>--<>--<>--<>--<>--<>"); //print results
        System.out.println("Course number: " + courseNumber);
        System.out.println("Department: " + department);
        System.out.println("Times per week: " + timesPerWeek);
        System.out.println("Start of class: " + time + " " + amOrPm);
        System.out.println("Intructor name: " + instructorName);
        System.out.println("Number of students: " + numStudents);
        
    } //end of method
} //end of class