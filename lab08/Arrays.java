import java.util.*;
public class Arrays{
    public static void main (String []args){ //main method
        Random rand = new Random(); //random num
        int num, counter = 0;
        String time = ""; //determine whether to display time or times
        
        int[] randomArray = new int[100]; //array of 100 numbers
        
        System.out.print("Array 1 holds the following integers: ");
        for(int i = 0; i < 100; i++){ //display the array
            num = rand.nextInt(100);
            randomArray[i] = num;
            System.out.print(randomArray[i] + " ");
        }
        System.out.println(); //scan each int in the array to check for how many duplicates there are
        for(int j = 0; j < randomArray.length; j++){ //number to scan for in the array
            counter = 0;
            for(int k = 0; k < randomArray.length; k++){ //scans through the array for j
                if(randomArray[k] == j){
                    counter++; //increase number if the values equal
                }
            }
            if(counter == 1) time = "time";
            else time = "times";    
            System.out.println(j + " occurs " + counter + " " + time + ".");
        }
    }//end main method
}//end class