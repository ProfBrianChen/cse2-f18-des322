import java.util.Scanner;
public class EncryptedX{
    //main method
    public static void main (String []args){
        String answer = " ";
        int input, counter = 1; 
        Scanner reader = new Scanner(System.in);

        while(true){ //checks if in range from 0-100
            while(true){ //checks if integer
                System.out.print("Enter an integer from 0-100: ");
                answer=reader.nextLine(); //input number for rows & columns
                Scanner stringReader = new Scanner(answer);
                if(stringReader.hasNextInt()){
                    break;
                }
                else{
                    System.out.println("Error: Enter an integer.");
                }
            }
            input = Integer.valueOf(answer);
            if(input>0 && input<=100){
                break;
            }
            else{
                System.out.println("Error: Out of bounds.");
            }
        }
        for(int i=0; i < input; i++){ //columns
            for(int j = 0; j < input; j++){ //rows
                if(i==j){
                    System.out.print(" ");
                }
                else if(input-j==counter){
                    System.out.print(" ");
                }
                else{
                    System.out.print("*");
                }

            }
            System.out.println();
            counter++;
        }
    }
}