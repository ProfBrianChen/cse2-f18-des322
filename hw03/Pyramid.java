import java.util.Scanner;
//Dylan Staniszewski
//September 16, 2018
//This program takes as input dimensions of a square base pyramid and calculates volume
public class Pyramid{
    //main method for every java program
    public static void main (String []args){
        double side;//length of side of base
        double height; //height of pyramid
        double volume; //total volume
        Scanner reader = new Scanner(System.in);
        
        System.out.print("Enter a length for the side of the base: ");
        side = reader.nextDouble(); //input side length
        System.out.print("Enter a height for the pyramid: ");
        height = reader.nextDouble(); //input height
        
        volume = Math.pow(side, 2.0) * (height / 3.0); //calculates volume
        
        System.out.println("The volume of the pyramid is: " + volume); //display results
    } //end of main method
} //end of class