//Dylan Staniszewski
//September 16, 2018
//This program takes as input total affected area and rainfall to calculate how many cubic miles of rain fell.
import java.util.Scanner;
public class Convert{
    //main method for every java program
    public static void main (String []args){
        double affectedArea; 
        double rainfall; //inches of rainfall
        double totalMiles;
        Scanner reader = new Scanner(System.in);
        
        System.out.print("Enter the total affected area in acres: ");
        affectedArea = reader.nextDouble(); //input affected area
        System.out.print("Enter the total rainfall in inches: ");
        rainfall = reader.nextDouble(); //input total rainfall
        
        //1 acre = 43560 square foot
        
        affectedArea = (affectedArea * 43560) * Math.pow(12.0, 2.0); //converts acres to feet, and then to inches
        
        totalMiles = ((affectedArea * rainfall) / Math.pow(12.0, 3.0)) / Math.pow(5280.0, 3.); //calculate cubic miles
        
        System.out.println("Total: " + totalMiles + " cubic miles."); //print output
    }
}