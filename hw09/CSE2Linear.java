import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{
    public static void binarySearch(int[] a){
        Scanner reader = new Scanner(System.in);
        int grade, low = 0, high = a.length - 1, mid = (low+high)/2;
        int iterations = 0, wasFound = 0;
        System.out.print("Enter a grade to search for: ");
        grade = reader.nextInt();

        //This binary search is based off of Professor Carr's example
        //She said we were able to use it if we cited it
        while(high>=low){ 
            if(grade>a[mid]){ 
                low = mid + 1; 
                mid = (low+high)/2;
                iterations++;
            }
            else if(grade<a[mid]){
                high = mid - 1;
                mid = (low + high)/2;
                iterations++;
            }
            else if(grade==a[mid]){
                wasFound = 1;
                iterations++;
                System.out.println(grade + " was found in " + iterations + " iterations.");
                break;
            }
            else{
            }
        }
        if(wasFound==0){
            System.out.println(grade + " was not found in " + iterations + " iterations.");
        }
    }

    public static void linearSearch(int[] a){
        Scanner reader = new Scanner(System.in);
        int grade, wasFound = 0, iterations = 0;

        System.out.print("Enter a grade to search for: ");
        grade=reader.nextInt();
        
        for(int i = 0; i < 15; i ++){
            if(grade == a[i]){
                iterations++;
                System.out.println(grade + " was found with " + iterations + " iterations.");
                wasFound = 1;
                break;
            }
            else{
                iterations++;
            }
        }
        if(wasFound == 0) System.out.println(grade + " was not found with " + iterations + " iterations.");
    }

    public static void scramble(int[] a){
        Random rand = new Random();
        int swapPlace, temp = 0;
        Scanner reader = new Scanner(System.in);
        int grade, wasFound = 0, iterations = 0;
        
        for(int i = 0; i<100; i++){
            swapPlace = rand.nextInt(14)+1;
            temp = a[swapPlace];
            a[swapPlace] = a[0];
            a[0] = temp;
        }
        for(int j = 0; j<15; j++){
            System.out.print(a[j] + " ");
        }
        System.out.println();
        System.out.print("Enter a grade to search for: ");
        grade=reader.nextInt();
        for(int i = 0; i < 15; i ++){
            if(grade == a[i]){
                iterations++;
                System.out.println(grade + " was found with " + iterations + " iterations.");
                wasFound = 1;
                break;
            }
            else{
                iterations++;
            }
        }
        if(wasFound == 0) System.out.println(grade + " was not found with " + iterations + " iterations.");
    }

    public static void main (String []args){
        int[] grades = new int[15];
        int input = 0, i = 0, counter = 0;
        String grade;
        Scanner reader = new Scanner(System.in);

        while(counter < 15){
            System.out.print("Enter grade " + (counter+1) + ": ");
            grade=reader.nextLine();
            Scanner gradeReader = new Scanner(grade);
            if(gradeReader.hasNextInt()){
                input = Integer.valueOf(grade);
                if(input > 100 || input < 0){
                    System.out.println("Error: Out of bounds.");
                }
                else{
                    if((counter > 0) && (grades[counter-1] >= input)){
                        System.out.println("Error: Grade is less than or equal to previous input."); 
                    }
                    else{
                        grades[counter] = input;
                        counter++;
                    }
                }
            }
            else System.out.println("Error: Enter an integer.");
        }
        System.out.println();
        for(int j=0; j<15; j++){
            System.out.print(grades[j] + " ");
        }
        System.out.println();
        binarySearch(grades);
        System.out.println("-----------");
        linearSearch(grades);
        System.out.println("-----------");
        scramble(grades);
    }
}