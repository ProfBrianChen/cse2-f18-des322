import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
    public static int[] randomInput(){
        Random rand = new Random();
        int[] array = new int[10];
        int num;
        for(int i=0; i<10; i++){
            num = rand.nextInt(10);
            array[i] = num;
        }
        return array;
    }

    public static int[] delete(int[] list, int pos){

        int subtract = 0;
        if(pos>list.length || pos<0){
            System.out.println("The index is not valid.");
            int[] array = new int[list.length];
            for(int j = 0; j<list.length; j++){
                array[j] = list[j];
            }
            return array;
        }
        else{
            System.out.println("Index element " + pos + " removed.");
            int[] array = new int[list.length-1];
            for(int i=0; i<10; i++){
                if(i == pos){
                    subtract = 1;
                }
                else{
                    array[i-subtract] = list[i];
                }
            }
            return array;
        }

    }

    public static int[] remove(int[] list, int target){
        int numCounter = 0, subtract = 0;
        for(int i = 0; i<10; i++){
            if(list[i] == target){
                numCounter++;
            }
            else{
            }
        }
        if(numCounter ==0){
            System.out.println("Target value not found.");
            int[]array = new int[list.length];
            for(int j = 0; j<list.length; j++){
                array[j] = list[j];
            }
            return array;
        }
        else{
            System.out.println("Element " + target + " has been found.");
            int[] array = new int[list.length-numCounter];
            for(int j=0; j<array.length; j++){
                if(list[j] == target){
                    subtract++;
                }
                else{
                    array[j-subtract] = list[j]; 
                }
            }
            return array;
        }
    }
    

    public static void main(String [] args){
        Scanner scan=new Scanner(System.in);
        int num[]=new int[10];
        int newArray1[];
        int newArray2[];
        int index,target;
        String answer="";
        do{
            System.out.print("Random input 10 ints [0-9]");
            num = randomInput();
            String out = "The original array is:";
            out += listArray(num);
            System.out.println(out);

            System.out.print("Enter the index ");
            index = scan.nextInt();
            newArray1 = delete(num,index);
            String out1="The output array is ";
            out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
            System.out.println(out1);

            System.out.print("Enter the target value ");
            target = scan.nextInt();
            newArray2 = remove(num,target);
            String out2="The output array is ";
            out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer=scan.next();
        }while(answer.equals("Y") || answer.equals("y"));
    }

    public static String listArray(int num[]){
        String out="{";
        for(int j=0;j<num.length;j++){
            if(j>0){
                out+=", ";
            }
            out+=num[j];
        }
        out+="} ";
        return out;
    }
}
