
public class Arithmetic{
  public static void main (String []args){
      int numPants = 3; //number of pants
      double pantsPrice = 34.98; //price of pants
      int numShirts = 2; //number of shirts
      double shirtPrice = 24.99; //price of shirts
      int numBelts = 1; //number of belts
      double beltCost = 33.99; //cost of belts
      double salesTax = 0.06; //tax rate
    
    //total costs
      double totPants = numPants * pantsPrice; //pants total
      double totShirts = numShirts * shirtPrice;//shirts total
      double totBelts = numBelts * beltCost;//belts total
      double total = totPants + totShirts + totBelts; //total without tax
      double taxPants = totPants * salesTax; //pants tax
      double taxShirts = totShirts * salesTax; //shirts tax
      double taxBelts = totBelts * salesTax; //belts tax
    //making tax costs only have two decimal places
      taxPants = ((int)(taxPants * 100)) / 100.0;
      taxShirts = ((int)(taxShirts * 100)) / 100.0;
      taxBelts = ((int)(taxBelts * 100)) / 100.0;
      double totTax = taxPants + taxShirts + taxBelts; //total tax
      double grandTotal = total + totTax; //grand total
    
    
    
    System.out.println("Total price and tax of pants: $" + totPants + " (price), $" + taxPants + " (tax)");
    System.out.println("Total price and tax of shirts: $" + totShirts + " (price), $" + taxShirts + " (tax)");
    System.out.println("Total price and tax of belts: $" + totBelts + " (price), $" + taxBelts + " (tax)");
    System.out.println("--------");
    System.out.println("Total price before tax: $" + total);
    System.out.println("Total tax: $" + totTax);
    System.out.println("Grand total: $" + grandTotal);
  }
}