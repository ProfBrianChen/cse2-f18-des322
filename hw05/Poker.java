import java.util.Scanner;
import java.util.Random;
public class Poker{
  //main method
    public static void main (String []args){
        int numHands, onePair = 0, twoPair = 0, threeKind = 0, fourKind = 0, counter = 0, counter2 = 0;
        int card1Counter=0, card2Counter=0, card3Counter=0, card4Counter=0;
        Scanner reader = new Scanner(System.in);
        Random rand = new Random();
        int card1, card2, card3, card4, card5; //cards
        

        System.out.print("Input number of hands to be generated: ");
        numHands = reader.nextInt(); //input hands

        while(counter<numHands){ //continually generate new hands of five cards and check for conditions for as many hands as were chosen
            card1Counter = 0;
            card2Counter = 0;
            card3Counter = 0;
            card4Counter = 0;
            
            card1 = rand.nextInt(52) + 1; //gen card1
            do{
                card2 = rand.nextInt(52) + 1; //gen card2 
            }while(card2 == card1);
            do{
                card3 = rand.nextInt(52) + 1; //gen card3
            }while((card3 == card2) && (card3 == card1));
            do{
                card4 = rand.nextInt(52) + 1; //gen card4 
            }while((card4 == card3) && (card4 ==card2) && (card4 == card1));
            do{
                card5 = rand.nextInt(52) + 1; //gen card5 
            }while((card5 == card4) && (card5 == card3) && (card5 == card2) && (card5 == card1));
            
            card1 = card1 % 13;
            card2 = card2 % 13;
            card3 = card3 % 13;
            card4 = card4 % 13;
            card5 = card5 % 13;
            
          //check for one pair
            if((card1 == card2) && ((card1 != card3) && (card1 != card4) && (card1 != card5))){
                onePair++;
                card1Counter = 1;
            }
            if((card1 == card3) && ((card1 != card2) && (card1 != card4) && (card1 != card5))){
                onePair++;
                card1Counter = 1;
            }
            if((card1 == card4) && ((card1 != card2) && (card1 != card3) && (card1 != card5))){
                onePair++;
                card1Counter = 1;
            }
            if((card1 == card5) && ((card1 != card2) && (card1 != card4) && (card1 != card3))){
                onePair++;
                card1Counter = 1;
            }
            
            if((card2 == card3) && ((card2 != card1) && (card2 != card4) && (card2 != card5))){
                onePair++;
                card2Counter = 1;
            }
            if((card2 == card4) && ((card2 != card1) && (card2 != card3) && (card2 != card5))){
                onePair++;
                card2Counter = 1;
            }
            if((card2 == card5) && ((card2 != card1) && (card2 != card3) && (card2 != card4))){
                onePair++;
                card2Counter = 1;
            }
            
            if((card3 == card4) && ((card3 != card1) && (card3 != card2) && (card3 != card5))){
                onePair++;
                card3Counter = 1;
            }
            if((card3 == card5) && ((card3 != card2) && (card3 != card4) && (card3 != card1))){
                onePair++;
                card3Counter = 1;
            }
            
            if((card4 == card5) && ((card4 != card1) && (card4 != card2) && (card4 != card3))){
                onePair++;
                card4Counter = 1;
            }
            
            if(card1Counter + card2Counter + card3Counter + card4Counter == 2){
                twoPair++;
            }
            
          //check for three of a kind
            if(((card1 == card2) && (card1 == card3)) && ((card1 != card4) && (card1 != card5))) threeKind++;
            if(((card1 == card2) && (card1 == card4)) && ((card1 != card3) && (card1 != card5))) threeKind++;
            if(((card1 == card2) && (card1 == card5)) && ((card1 != card4) && (card1 != card3))) threeKind++;
            if(((card1 == card3) && (card1 == card4)) && ((card1 != card2) && (card1 != card5))) threeKind++;
            if(((card1 == card3) && (card1 == card5)) && ((card1 != card4) && (card1 != card2))) threeKind++;
            if(((card1 == card4) && (card1 == card5)) && ((card1 != card2) && (card1 != card3))) threeKind++;
            
            if(((card2 == card3) && (card2 == card4)) && ((card2 != card1) && (card2 != card5))) threeKind++;
            if(((card2 == card3) && (card2 == card5)) && ((card2 != card1) && (card2 != card4))) threeKind++;
            if(((card2 == card4) && (card2 == card5)) && ((card2 != card1) && (card2 != card3))) threeKind++;
            
            if(((card3 == card4) && (card3 == card5)) && ((card3 != card1) && (card3 != card2))) threeKind++;
            
            //check for four of a kind
            if(((card1 == card2) && (card1 == card3) && (card1 == card4)) && (card1 != card5)) fourKind++;
            if(((card1 == card2) && (card1 == card3) && (card1 == card5)) && (card1 != card4)) fourKind++;
            if(((card1 == card2) && (card1 == card5) && (card1 == card4)) && (card1 != card3)) fourKind++;
            if(((card1 == card5) && (card1 == card3) && (card1 == card4)) && (card1 != card2)) fourKind++;
            
            if(((card2 == card3) && (card2 == card4) && (card2 == card5)) && (card2 != card1)) fourKind++;
            
            counter++;
        }
      //print results
        System.out.println("Number of hands generated: " + numHands);
        System.out.println("The probability of Four-of-a-kind: " + ((double)fourKind/numHands));
        System.out.println("The probability of Three-of-a-kind: " + ((double)threeKind/numHands));
        System.out.println("The probability of Two-pair: " + ((double)twoPair/numHands));
        System.out.println("The probability of One-pair: " + ((double)onePair/numHands));
    } //end method
} //end class