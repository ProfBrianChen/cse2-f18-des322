import java.util.Scanner;
public class PatternC{
    public static void main (String []args){
        int rows, num, input, counter;
        Scanner reader = new Scanner(System.in);
        
        do{
            System.out.print("Enter number of rows: ");
            input=reader.nextInt();
        }while((input < 1) && (input > 10));
        
        for(rows = 1; rows < (input+1); rows++){
            for(counter=1; counter < (input - rows + 1); counter++){
                    System.out.print(" ");
                }
            for(num = rows; num > 0; num--){
                System.out.print(num);
            }
            System.out.println();
        }
    }
}