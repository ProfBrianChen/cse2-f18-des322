import java.util.Scanner;
public class PatternD{
    public static void main (String []args){
        int rows, num, input;
        Scanner reader = new Scanner(System.in);
        
        do{
            while(true){
            System.out.print("Enter number of rows: ");
            input=reader.nextInt();
            Scanner rowCheck = new Scanner(input);
            if(rowCheck.hasNextInt()){
              break;
            }
          else{
            System.out.println("Error: Enter an integer.");
          }
            }
        }while((input < 1) && (input > 10));
        
        for(rows = input; rows > 0; rows--){
            for(num = rows; num > 0; num --){
                System.out.print(num + " ");
            }
            System.out.println();
        }
    }
}