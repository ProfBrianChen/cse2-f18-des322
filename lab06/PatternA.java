import java.util.Scanner;
public class PatternA{
    public static void main (String []args){
        int rows, num, input;
        Scanner reader = new Scanner(System.in);

        do{
            System.out.print("Enter number of rows: ");
            input=reader.nextInt();
        }while((input < 1) && (input > 10));
        
        for(rows = 1; rows < (input+1); rows++){
            for(num = 1; num < (rows+1); num++){
                System.out.print(num + " ");
            }
            System.out.println();
        }
    }
}